package si.fri.prpo.DTOObjects;

public class OpomnikDTO {

    private int uporabnikID;
    private int lokacijaID;
    private int opomnikID;
    private int storitevID;

    private String address;


    private String naslov;
    private String opis;
    private String status;
    private String atribut;

    public void setUporabnikID(int uporabnikID){
        this.uporabnikID = uporabnikID;
    }

    public void setLokacijaID(int lokacijaID){
        this.lokacijaID = lokacijaID;
    }

    public void setOpomnikID(int opomnikID) {
        this.opomnikID = opomnikID;
    }

    public void setAtribut(String atribut) {
        this.atribut = atribut;
    }

    public String getAtribut() {
        return atribut;
    }

    public int getUporabnikID(){
        return uporabnikID;
    }

    public void setopomnikID(int opomnikID){
        this.opomnikID = opomnikID;
    }

    public int getOpomnikID(){
        return opomnikID;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public int getLokacijaID(){
        return lokacijaID;
    }

    public String getNaslov(){
        return naslov;
    }

    public String getOpis(){
        return opis;
    }

    public void setNaslov(String naslov){
        this.naslov = naslov;
    }

    public void setOpis(String opis){
        this.opis = opis;
    }

    public void setStoritevID(int storitevID) {
        this.storitevID = storitevID;
    }

    public int getStoritevID() {
        return storitevID;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
