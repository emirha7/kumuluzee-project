package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import si.fri.prpo.entitete.Lokacija;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.DTOObjects.LokacijaDTO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class UpravljanjeLokacijaZrno {

    @Inject
    private LokacijaZrno lokacijaZrno;

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    private static Logger logger = Logger.getLogger(UpravljanjeLokacijaZrno.class.getName());

    @PreDestroy
    public void destroy(){
        logger.info("upravljanjeLokacijaZrno ubito");
    }

    @PostConstruct
    public void create(){
        logger.info("upravljanjeLokacijaZrno ustvarjeno");
    }

    public List<Lokacija> vrniLokacije(QueryParameters query){
        return lokacijaZrno.getAll(query);
    }

    @Transactional
    public int addLocation(LokacijaDTO lokacijaDTO){
        Uporabnik uporabnik = uporabnikiZrno.pridobiUporabnika(lokacijaDTO.getUporabnikID());
        if(uporabnik == null){
            logger.info("uporabnika z idjem "+lokacijaDTO.getUporabnikID() +" ne najdem");
            return -1;
        }

        Lokacija l = lokacijaZrno.pridobiLokacijo(lokacijaDTO.getSirina(), lokacijaDTO.getDolzina());
        if(l != null){
            logger.info("LOKACIJA ZE OBSTAJA");
            return l.getId();
        }
        Lokacija novaLokacija = new Lokacija();
        novaLokacija.setRadij(lokacijaDTO.getRadij());
        novaLokacija.setSirina(lokacijaDTO.getSirina());
        novaLokacija.setDolzina(lokacijaDTO.getDolzina());

        lokacijaZrno.dodajLokacijo(novaLokacija);
        logger.info("Lokacija "+novaLokacija.toString() +" uspesno dodana");
        l = lokacijaZrno.pridobiLokacijo(lokacijaDTO.getSirina(), lokacijaDTO.getDolzina());
        return l.getId();
    }

}
