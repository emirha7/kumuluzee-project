package si.fri.prpo.customExceptions;

import javax.persistence.NoResultException;

public class NoServiceException extends NoResultException {

    public NoServiceException(){
        super();
    }
}
