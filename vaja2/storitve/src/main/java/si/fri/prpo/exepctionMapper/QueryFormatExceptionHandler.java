package si.fri.prpo.exepctionMapper;

import com.kumuluz.ee.rest.exceptions.QueryFormatException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class QueryFormatExceptionHandler extends Exception implements ExceptionMapper<QueryFormatException> {

    @Override
    public Response toResponse(QueryFormatException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity("{\n" +
                "  \"errorMessage\": \"Query exception\",\n" +
                "  \"type\": \"QueryFormatException\"\n" +
                "}").build();
    }
}
