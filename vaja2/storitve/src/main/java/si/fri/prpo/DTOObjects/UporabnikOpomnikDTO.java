package si.fri.prpo.DTOObjects;


import si.fri.prpo.entitete.Opomnik;
import si.fri.prpo.entitete.Uporabnik;

import java.util.List;

public class UporabnikOpomnikDTO {

    private Uporabnik uporabnik;
    private List<Opomnik> opomnikList;
    private int id;
    private String ime;
    private String priimek;
    private String uporabniskoIme;


    public UporabnikOpomnikDTO(Uporabnik uporabnik, List<Opomnik> opomnikList){
        this.id = uporabnik.getId();
        this.ime = uporabnik.getIme();
        this.priimek = uporabnik.getPriimek();
        this.uporabniskoIme = uporabnik.getUporabniskoIme();
        this.opomnikList = opomnikList;
    }

    public Uporabnik getUporabnik() {
        return uporabnik;
    }

    public void setOpomnikList(List<Opomnik> opomnikList) {
        this.opomnikList = opomnikList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public String getUporabniskoIme() {
        return uporabniskoIme;
    }

    public void setUporabniskoIme(String uporabniskoIme) {
        this.uporabniskoIme = uporabniskoIme;
    }

    public void setUporabnik(Uporabnik uporabnik) {
        this.uporabnik = uporabnik;
    }

    public List<Opomnik> getOpomnikList() {
        return opomnikList;
    }



}
