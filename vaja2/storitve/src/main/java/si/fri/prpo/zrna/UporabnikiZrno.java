package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.customExceptions.UserNotFoundException;
import si.fri.prpo.entitete.Uporabnik;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class UporabnikiZrno {

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;
    private static Logger logger = Logger.getLogger(UporabnikiZrno.class.getName());

    @Inject
    private UUIDGenerator u;

    public List<Uporabnik> getUporabniki(QueryParameters query) {
        return JPAUtils.queryEntities(em, Uporabnik.class, query);
    }

    public List<Uporabnik> getByName(String ime) {
        return em.createNamedQuery("Uporabnik.getByName").setParameter("name", ime).getResultList();
    }

    public Uporabnik getByUsername(String username) {
        try {
            Query q = em.createNamedQuery("Uporabnik.getByUsername");
            q.setParameter("username", username);
            return (Uporabnik) q.getSingleResult();
        }catch (NoResultException e){
            throw new UserNotFoundException();
        }
    }


    public boolean login(String geslo, String username){
        Uporabnik uporabnik = getByUsername(username);
        if(uporabnik == null){
            logger.info("uporabnik z uporabniskim imenom "+username +" ne obstaja");
            return false;
        }
        return uporabnik.getGeslo().equals(geslo);
    }

    @PostConstruct
    public void create(){
        logger.info("uporabnikZrno USTVARJENO "+u.getUUID().toString());
    }

    @PreDestroy
    public void destroy(){
        logger.info("uporabnikZrno UNICENO "+u.getUUID().toString());
    }


    public Uporabnik pridobiUporabnika(int uporabnikId) {
        logger.info("pridobivam uporabnika z idjem "+uporabnikId);
        Uporabnik u = em.find(Uporabnik.class, uporabnikId);
        if(u == null){
            logger.info("pridobivanje uporabnika z idjem "+uporabnikId + " NEUSPESNO");
            throw new UserNotFoundException();
        }
        logger.info("pridobivanje uporabnika z idjem "+uporabnikId + "USPESNO");
        return u;
    }

    @Transactional
    public void dodajUporabnika(Uporabnik u){
        if(u!=null){
            em.persist(u);
        }else{
            logger.info("dodou uporabnika NEuspesno");
        }
    }

    @Transactional
    public void posodbiUporabnika(Uporabnik u){

        if(u!=null){
            logger.info("posodabljam uporabnika "+u.toString());
            Uporabnik updatedUporabnik  = em.merge(u);
            logger.info(updatedUporabnik.toString());
        }else {
            logger.info(" uporabnika  ni");
        }
    }

    @Transactional
    public void odstraniUporabnika(Uporabnik u) throws UserNotFoundException{
        if(u!=null){
            logger.info("odstranil uporabnika uspesno");
            em.remove(u);
        }else{
            logger.info("odstranil uporabnika NEuspesno");
            throw new UserNotFoundException();
        }
    }

    public List<Uporabnik> getAllCriteria() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Uporabnik> u = cb.createQuery(Uporabnik.class);
        Root<Uporabnik> c = u.from(Uporabnik.class);
        u.select(c);
        return em.createQuery(u).getResultList();
    }
}
