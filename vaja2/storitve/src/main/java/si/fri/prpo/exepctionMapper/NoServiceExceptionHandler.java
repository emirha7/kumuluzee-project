package si.fri.prpo.exepctionMapper;

import si.fri.prpo.customExceptions.NoServiceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NoServiceExceptionHandler extends Exception implements ExceptionMapper<NoServiceException> {

    @Override
    public Response toResponse(NoServiceException e) {
        return Response.status(Response.Status.NOT_FOUND).entity("{\n" +
                "\"errorMessage\": \"Object not found exception\",\n" +
                "\"type\": \"ServiceNotFoundException\" \n" +
                "}").build();
    }
}
