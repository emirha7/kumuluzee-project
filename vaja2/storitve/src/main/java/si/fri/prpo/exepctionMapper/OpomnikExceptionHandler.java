package si.fri.prpo.exepctionMapper;

import si.fri.prpo.customExceptions.OpomnikNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class OpomnikExceptionHandler extends Throwable implements ExceptionMapper<OpomnikNotFoundException> {

    @Override
    public Response toResponse(OpomnikNotFoundException e) {

        return Response.status(Response.Status.NOT_FOUND).entity("{\n" +
                "\"errorMessage\": \"Object not found exception\",\n" +
                "\"type\": \"OpomnikNotFoundException\" \n" +
                "}").build();
    }
}
