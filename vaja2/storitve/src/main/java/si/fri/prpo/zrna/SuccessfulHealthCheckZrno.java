package si.fri.prpo.zrna;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheckResponse;

@Health
public class SuccessfulHealthCheckZrno {
    public HealthCheckResponse call() {
        return HealthCheckResponse.named(SuccessfulHealthCheckZrno.class.getSimpleName()).up().build();
    }
}
