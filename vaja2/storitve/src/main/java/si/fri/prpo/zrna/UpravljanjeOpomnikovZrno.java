package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import si.fri.prpo.DTOObjects.OpomnikStoritevDTO;
import si.fri.prpo.entitete.Lokacija;
import si.fri.prpo.entitete.Opomnik;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.DTOObjects.OpomnikDTO;
import si.fri.prpo.DTOObjects.UporabnikDTO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class UpravljanjeOpomnikovZrno {

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    @Inject
    private UpravljanjeUporabnikovZrno uuz;

    @Inject
    private OpomnikZrno opomnikZrno;

    @Inject
    private LokacijaZrno lokacijaZrno;

    @Inject
    private UUIDGenerator u;

    private static Logger logger = Logger.getLogger(UpravljanjeOpomnikovZrno.class.getName());

    @PostConstruct
    public void create(){
        logger.info("UpravljanjeOpomnikovZrno USTVARJENO "+u.getUUID().toString());
    }

    @PreDestroy
    public void destroy(){
        logger.info("UpravljanjeOpomnikovZrno USTVARJENO ");
    }

    @Transactional
    public boolean dodajOpomnik(OpomnikDTO opomnikDTO){

        Uporabnik uporabnik = uporabnikiZrno.pridobiUporabnika(opomnikDTO.getUporabnikID());
        if(uporabnik!=null){
            logger.info("uporabnik ni null ampak "+uporabnik.toString());
        }else{
            logger.info("uporabnik je null");
            return false;
        }
        Lokacija lokacija = lokacijaZrno.pridobiLokacijo(opomnikDTO.getLokacijaID());
        if(lokacija!=null){
            logger.info("lokacija ni null ampak "+lokacija.toString());
        }else{
            logger.info("lokacija je null");
            return false;
        }

        Opomnik novOpomnik = new Opomnik();
        novOpomnik.setOpis(opomnikDTO.getOpis());
        novOpomnik.setNaslov(opomnikDTO.getNaslov());
        novOpomnik.setLokacija(lokacija);
        novOpomnik.setUporabnik(uporabnik);
        novOpomnik.setStatus("todo");
        opomnikZrno.dodajOpomnik(novOpomnik);
        novOpomnik.setStoritev_id(opomnikDTO.getStoritevID());
        logger.info("opomnik "+novOpomnik.toString()+" je uspešno dodan");
        return true;

    }

    public List<OpomnikStoritevDTO> vrniOpomnikeStoritev(QueryParameters query){
        List<Opomnik> opomnikList = opomnikZrno.getAll(query);
        List<OpomnikStoritevDTO> opomnikStoritevDTOS = new ArrayList<OpomnikStoritevDTO>();


        for(Opomnik o : opomnikList){
            opomnikStoritevDTOS.add(opomnikZrno.pridobiOpomnikStoritev(o.getId()));
        }

        /*System.out.println("RETURNAM SLEDECE: ");
        for (OpomnikStoritevDTO o: opomnikStoritevDTOS){
            System.out.println(o.getNaslovOpomnika() + " "+o.getId());
        }
        System.out.println("vracam result dolzine "+opomnikStoritevDTOS.size());*/
        return opomnikStoritevDTOS;
    }

    public List<Opomnik> vrniOpomnike(QueryParameters query){
        return opomnikZrno.getAll(query);
    }


    @Transactional
    public boolean izbrisiOpomnik(int id){
        Opomnik opomnik = opomnikZrno.pridobiOpomnik(id);
        if(opomnik == null){
            logger.info("Opomnik z idjem "+id+ "ne obstaja");
            return false;
        }
        opomnikZrno.odstraniOpomnik(opomnik);
        return true;
    }

    @Transactional
    public void posodobiOpomnik(OpomnikDTO opomnikDTO){

        Opomnik opomnik = opomnikZrno.pridobiOpomnik(opomnikDTO.getOpomnikID());
        switch (opomnikDTO.getAtribut()){
            case "naslov": opomnik.setNaslov(opomnikDTO.getNaslov()); break;
            case "opis": opomnik.setOpis(opomnikDTO.getOpis()); break;
            case "status": opomnik.setStatus(opomnikDTO.getStatus()); break;
            case "lokacija": opomnik.setLokacija(lokacijaZrno.pridobiLokacijo(opomnikDTO.getLokacijaID())); break;
            case "storitevID": opomnik.setStoritev_id(opomnikDTO.getStoritevID()); break;
        }
        opomnikZrno.posodbiOpomnik(opomnik);

    }

}
