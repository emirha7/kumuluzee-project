package si.fri.prpo.DTOObjects;

public class UporabnikDTO {

    private String uporabniskoIme;
    private String geslo;
    private String ime;
    private String priimek;
    private String email;
    private String atribut;

    public String getAtribut() {
        return atribut;
    }

    public void setAtribut(String atribut) {
        this.atribut = atribut;
    }

    public String getUporabniskoIme(){
        return uporabniskoIme;
    }

    public void setUporabniskoIme(String uporabniskoIme){
        this.uporabniskoIme = uporabniskoIme;
    }

    public String getGeslo(){
        return geslo;
    }

    public void setGeslo(String geslo){
        this.geslo =  geslo;
    }

    public String getIme(){
        return ime;
    }

    public void setIme(String ime){
        this.ime =  ime;
    }

    public String getPriimek(){
        return priimek;
    }

    public void setPriimek(String priimek){
        this.priimek =  priimek;
    }

    public void setEmail(String email){
        this.email =  email;
    }

    public String getEmail(){
        return email;
    }

    @Override
    public String toString() {
        return "UporabnikDTO{" +
                "username='" + uporabniskoIme + '\'' +
                ", geslo='" + geslo + '\'' +
                ", ime='" + ime + '\'' +
                ", priimek='" + priimek + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

}