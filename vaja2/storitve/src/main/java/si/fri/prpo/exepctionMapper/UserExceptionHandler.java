package si.fri.prpo.exepctionMapper;

import si.fri.prpo.customExceptions.UserNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserExceptionHandler extends Throwable implements ExceptionMapper<UserNotFoundException> {

    @Override
    public Response toResponse(UserNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).entity("{\n" +
                "\"errorMessage\": \"Object not found exception\",\n" +
                "\"type\": \"UserNotFoundException\" \n" +
                "}").build();
    }
}
