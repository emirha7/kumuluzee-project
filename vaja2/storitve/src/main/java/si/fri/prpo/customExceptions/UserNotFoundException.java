package si.fri.prpo.customExceptions;

import javax.persistence.NoResultException;

public class UserNotFoundException extends NoResultException {

    public UserNotFoundException(){
        super();
    }
}
