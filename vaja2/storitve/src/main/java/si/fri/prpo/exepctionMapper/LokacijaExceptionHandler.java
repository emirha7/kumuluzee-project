package si.fri.prpo.exepctionMapper;

import si.fri.prpo.customExceptions.LokacijaNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class LokacijaExceptionHandler extends Throwable implements ExceptionMapper<LokacijaNotFoundException> {

    @Override
    public Response toResponse(LokacijaNotFoundException e) {
        return Response.status(Response.Status.NOT_FOUND).entity("{\n" +
                "\"errorMessage\": \"Object not found exception\",\n" +
                "\"type\": \"LokacijaNotFoundException\" \n" +
                "}").build();
    }

}
