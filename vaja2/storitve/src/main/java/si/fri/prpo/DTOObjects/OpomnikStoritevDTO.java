package si.fri.prpo.DTOObjects;

import si.fri.prpo.entitete.Lokacija;
import si.fri.prpo.entitete.Opomnik;

public class OpomnikStoritevDTO {

    private int id;
    private Lokacija lokacija;
    private int uporabnikID;
    private String naslovOpomnika;
    private String opisOpomnika;
    private String status;

    private String opisStoritve;
    private String naslovStoritve;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Lokacija getLokacija() {
        return lokacija;
    }

    public void setLokacija(Lokacija lokacija) {
        this.lokacija = lokacija;
    }

    public void setUporabnikID(int uporabnikID) {
        this.uporabnikID = uporabnikID;
    }

    public void setNaslovOpomnika(String naslovOpomnika) {
        this.naslovOpomnika = naslovOpomnika;
    }

    public void setOpisOpomnika(String opisOpomnika) {
        this.opisOpomnika = opisOpomnika;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOpisStoritve(String opisStoritve) {
        this.opisStoritve = opisStoritve;
    }

    public void setNaslovStoritve(String naslovStoritve) {
        this.naslovStoritve = naslovStoritve;
    }

    public int getUporabnikID() {
        return uporabnikID;
    }

    public String getNaslovOpomnika() {
        return naslovOpomnika;
    }

    public String getOpisOpomnika() {
        return opisOpomnika;
    }

    public String getStatus() {
        return status;
    }

    public String getOpisStoritve() {
        return opisStoritve;
    }

    public String getNaslovStoritve() {
        return naslovStoritve;
    }

    public OpomnikStoritevDTO(Opomnik o, StoritevDTO s){
        id = o.getId();
        lokacija = o.getLokacija();
        uporabnikID = o.getUporabnik().getId();
        naslovOpomnika = o.getNaslov();
        opisOpomnika = o.getOpis();
        status = o.getStatus();
        opisStoritve = s.getDescription();
        naslovStoritve = s.getTitle();
    }
}
