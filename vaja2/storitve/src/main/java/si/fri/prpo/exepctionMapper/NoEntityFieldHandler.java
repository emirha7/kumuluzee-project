package si.fri.prpo.exepctionMapper;

import com.kumuluz.ee.rest.exceptions.NoSuchEntityFieldException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NoEntityFieldHandler extends Exception implements ExceptionMapper<NoSuchEntityFieldException> {
    @Override
    public Response toResponse(NoSuchEntityFieldException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity("{\n" +
                "  \"errorMessage\": \"Field not found\",\n" +
                "  \"type\": \"NoSuchEntityFieldException\"\n" +
                "}").build();
    }
}
