package si.fri.prpo.zrna;

import com.kumuluz.ee.configuration.cdi.ConfigBundle;
import com.kumuluz.ee.configuration.cdi.ConfigValue;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
@ConfigBundle("rest-config")
public class ETCDZrno {

    @ConfigValue(watch= true)
    private Boolean ponudnikEnabled;

    @ConfigValue(watch= true)
    private String ponudnikUrl;

    public String getPonudnikUrl() {
        return ponudnikUrl;
    }

    public void setPonudnikUrl(String ponudnikUrl) {
        this.ponudnikUrl = ponudnikUrl;
    }

    public Boolean getPonudnikEnabled() {
        return ponudnikEnabled;
    }

    public void setPonudnikEnabled(Boolean ponudnikEnabled) {
        this.ponudnikEnabled = ponudnikEnabled;
    }
}
