package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.exceptions.NoSuchEntityFieldException;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.customExceptions.LokacijaNotFoundException;
import si.fri.prpo.entitete.Lokacija;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class LokacijaZrno {

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;


    @Inject
    private UUIDGenerator u;

    private static Logger logger = Logger.getLogger(UporabnikiZrno.class.getName());

    public List<Lokacija> getAll(QueryParameters query) {
            return JPAUtils.queryEntities(em, Lokacija.class, query);
    }

    public List<Lokacija> getByRadius(Double radij) {
        return em.createNamedQuery("Lokacija.getByRadius").setParameter("rad", radij).getResultList();
    }

    @PostConstruct
    public void create(){
        logger.info("lokacijaZrno USTVARJENO "+u.getUUID().toString());
    }

    @PreDestroy
    public void destroy(){
        logger.info("lokacijaZrno UNICENO "+u.getUUID().toString());
    }

    public Lokacija pridobiLokacijo(int lokacijaId) {
        logger.info("pridobivam uporabnika z idjem "+lokacijaId);
        Lokacija l = em.find(Lokacija.class, lokacijaId);

        if(l == null){
            logger.info("pridobivanje uporabnika z idjem "+lokacijaId + " NEUSPESNO");
            throw new LokacijaNotFoundException();
        }

        logger.info("pridobivanje lokacije z idjem "+lokacijaId + " USPESNO");
        return l;

    }

    public Lokacija pridobiLokacijo(double lat, double lon){
        logger.info("lat = "+lat +" long = "+lon);

        Lokacija lok = null;
        try{
            lok = (Lokacija)em.createNamedQuery("Lokacija.getByLatLong").setParameter("sirina", lat).setParameter("dolzina",lon).getSingleResult();
        }catch(NoResultException e){
            logger.info("lokacija obstaja");
            return null;
        }

        return lok;
    }

    @Transactional
    public void dodajLokacijo(Lokacija novaLokacija){
        if(novaLokacija!=null){
            logger.info("dodou lokacijo uspesno "+novaLokacija.toString());
            em.persist(novaLokacija);
        }else{
            logger.info("dodou lokacijo NEuspesno");
        }
    }



    @Transactional
    public void posodbiLokacijo(Lokacija lok){
        if(lok!=null){
            logger.info("posodabljam lokacijo "+lok.toString());
            Lokacija updatedLokacija  = em.merge(lok);
        }else {
            logger.info(" lokacije  ni");
        }
    }

    @Transactional
    public void odstraniLokacijo(Lokacija lok){

        if(!em.contains(lok)){
            lok = em.merge(lok);
        }

        em.remove(lok);

    }

    public List<Lokacija> getAllCriteria() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lokacija> l = cb.createQuery(Lokacija.class);
        Root<Lokacija> c = l.from(Lokacija.class);
        l.select(c);
        return em.createQuery(l).getResultList();
    }
}
