package si.fri.prpo.DTOObjects;

public class LokacijaDTO {

    private int uporabnikID;
    private double sirina;
    private double dolzina;
    private double radij;

    public double getSirina() {
        return sirina;
    }

    public double getDolzina() {
        return dolzina;
    }

    public double getRadij() {
        return radij;
    }

    public int getUporabnikID() {
        return uporabnikID;
    }

    public void setUporabnikID(int uporabnikID) {
        this.uporabnikID = uporabnikID;
    }

    public void setSirina(double sirina) {
        this.sirina = sirina;
    }

    public void setDolzina(double dolzina) {
        this.dolzina = dolzina;
    }

    public void setRadij(double radij) {
        this.radij = radij;
    }
}
