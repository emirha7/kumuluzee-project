package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.DTOObjects.UporabnikDTO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class UpravljanjeUporabnikovZrno {

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    private static Logger logger = Logger.getLogger(UpravljanjeUporabnikovZrno.class.getName());


    @Inject
    private UUIDGenerator u;

    @PostConstruct
    public void create(){
        logger.info("upravljanjeUporabnikovZrno ZRNO USTVARJENO "+u.getUUID().toString());
    }

    @PreDestroy
    public void destroy(){
        logger.info("upravljanjeUporabnikovZrno ZRNO USTVARJENO ");
    }

    public Uporabnik pridobiUporabnikaByUsername(UporabnikDTO uporabnikDTO){
        Uporabnik uporabnik= uporabnikiZrno.getByUsername(uporabnikDTO.getUporabniskoIme());
        if(uporabnik == null){
            logger.info("uporabnik z uporabniskim imenom: "+uporabnikDTO.getUporabniskoIme() +" ne obstaja");
            throw new NotFoundException();
        }
        logger.info("uporabnik  " +uporabnikDTO.getUporabniskoIme()+ " obstaja");
        return uporabnik;
    }

    public Uporabnik pridobiUporabnikaByID(int id){
        Uporabnik uporabnik= uporabnikiZrno.pridobiUporabnika(id);
        if(uporabnik == null){
            logger.info("uporabnik z uporabniskim imenom: "+id+" ne obstaja");
            throw new NotFoundException();
        }
        logger.info("uporabnik  " +id+ " obstaja");
        return uporabnik;
    }

    @Transactional
    public boolean dodajUporabnika(UporabnikDTO uporabnikDTO){
        Uporabnik uporabnik = new Uporabnik();
        uporabnik.setEmail(uporabnikDTO.getEmail());
        uporabnik.setUporabniskoIme(uporabnikDTO.getUporabniskoIme());
        uporabnik.setGeslo(uporabnikDTO.getGeslo());
        uporabnik.setIme(uporabnikDTO.getIme());
        uporabnik.setPriimek(uporabnikDTO.getPriimek());
        uporabnikiZrno.dodajUporabnika(uporabnik);
        logger.info("Uporabnik "+uporabnik.toString() + "je bil uspesno dodan");
        return true;
    }

    public List<Uporabnik> vrniUporabnike(QueryParameters query){
        return uporabnikiZrno.getUporabniki(query);
    }

    public boolean canLogin(UporabnikDTO uporabnikDTO){
        if(uporabnikiZrno.login(uporabnikDTO.getGeslo(),uporabnikDTO.getUporabniskoIme())){
            logger.info("Uporabnik "+uporabnikDTO.getUporabniskoIme() + "se lahko prijavi");
            return true;
        }
        logger.info("Uporabnik "+uporabnikDTO.getUporabniskoIme() + "geslo se ne ujema");
        return false;
    }

    @Transactional
    public void odstraniUporabnika(int id){
        uporabnikiZrno.odstraniUporabnika(uporabnikiZrno.pridobiUporabnika(id));
    }

    @Transactional
    public void updateUporabnik(UporabnikDTO uporabnikDTO){

        Uporabnik u = uporabnikiZrno.getByUsername(uporabnikDTO.getUporabniskoIme());

        switch (uporabnikDTO.getAtribut()){
            case "geslo": u.setGeslo(uporabnikDTO.getGeslo()); break;
            case "username": u.setUporabniskoIme(uporabnikDTO.getUporabniskoIme()); break;
            case "email": u.setEmail(uporabnikDTO.getEmail()); break;
            case "priimek": u.setPriimek(uporabnikDTO.getPriimek()); break;
            case "ime": u.setIme(uporabnikDTO.getIme());  break;
        }

        uporabnikiZrno.posodbiUporabnika(u);
    }
}
