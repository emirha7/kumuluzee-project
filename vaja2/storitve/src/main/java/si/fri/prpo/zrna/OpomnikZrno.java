package si.fri.prpo.zrna;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.DTOObjects.OpomnikStoritevDTO;
import si.fri.prpo.DTOObjects.StoritevDTO;
import si.fri.prpo.customExceptions.OpomnikNotFoundException;
import si.fri.prpo.entitete.Opomnik;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@ApplicationScoped
public class OpomnikZrno {

    @Inject
    private UUIDGenerator u;

    @Inject
    private StoritevZrno storitevZrno;

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;

    private static Logger logger = Logger.getLogger(Opomnik.class.getName());

    public List<Opomnik> getAll(QueryParameters query) {
        return JPAUtils.queryEntities(em, Opomnik.class, query);
    }

    public List<Opomnik> getAll(){
        return em.createNamedQuery("Opomnik.getAll").getResultList();
    }

    public List<Opomnik> getByDescription(String s) {
        return em.createNamedQuery("Opomnik.getByDescription").setParameter("str", s).getResultList();
    }


    @PostConstruct
    public void create(){
        logger.info("opomnikZrno USTVARJENO "+u.getUUID().toString());
    }

    @PreDestroy
    public void destroy(){
        logger.info("opomnikZrno UNICENO "+u.getUUID().toString());
    }

    public Opomnik pridobiOpomnik(int opomnikId) {
        logger.info("pridobivam opomnik z idjem "+opomnikId);
        Opomnik o = em.find(Opomnik.class, opomnikId);
        if(o == null){
            logger.info("pridobivanje uporabnika z idjem "+opomnikId + " NEUSPESNO");
            throw new OpomnikNotFoundException();
        }

        logger.info("pridobivanje uporabnika z idjem "+opomnikId + " USPESNO");
        return o;
    }

    public OpomnikStoritevDTO pridobiOpomnikStoritev(int id) {
        Opomnik o = pridobiOpomnik(id);
        /*System.out.println("----------OPOMNIK---------");
        System.out.println(o);
        */
        StoritevDTO storitevDTO = storitevZrno.getServiceByID(o.getStoritev_id());

        /*System.out.println("--------STORITEV---------");
        System.out.println(storitevDTO.getId() + " "+storitevDTO.getTitle());
        */
        return new OpomnikStoritevDTO(o, storitevDTO);
    }

    public List<OpomnikStoritevDTO> pridobiOpomnikStoritevAll(){
        List<Opomnik> opomnikList = getAll();
        List<OpomnikStoritevDTO> result = new ArrayList<OpomnikStoritevDTO>();
        for(Opomnik o : opomnikList){
            result.add(pridobiOpomnikStoritev(o.getId()));
        }
        return result;
    }


    public List<Opomnik> pridobiOpomnikeByUporabnikID(int uporabnikID){
       return em.createNamedQuery("Opomnik.getByUporabnikID").setParameter("uporabnikID",uporabnikID).getResultList();
    }

    @Transactional
    public void dodajOpomnik(Opomnik novOpomnik){
        if(novOpomnik!=null){
            logger.info("dodou opomnik uspesno "+novOpomnik.toString());
            em.persist(novOpomnik);
        }else{
            logger.info("dodou opomnik NEuspesno");
        }
    }



    @Transactional
    public void posodbiOpomnik(Opomnik opomnik){
        if(opomnik!=null){
            logger.info("posodabljam opomnik "+opomnik.toString());
            Opomnik updatedOpomnik  = em.merge(opomnik);
        }else {
            logger.info(" Opomnika  ni");
        }
    }

    @Transactional
    public void odstraniOpomnik(Opomnik o){
        if(o!=null){
            logger.info("odstranil opomnik uspesno");
            em.remove(o);
        }else{
            logger.info("odstranil opomnik NEuspesno");
        }
    }

    public List<Opomnik> getAllCriteria() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Opomnik> o = cb.createQuery(Opomnik.class);
        Root<Opomnik> c = o.from(Opomnik.class);
        o.select(c);
        return em.createQuery(o).getResultList();
    }
}


