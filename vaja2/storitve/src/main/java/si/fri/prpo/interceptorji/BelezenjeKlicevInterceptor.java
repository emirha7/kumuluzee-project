package si.fri.prpo.interceptorji;

import si.fri.prpo.anotacije.BeleziKlice;
import si.fri.prpo.zrna.BelezenjeKlicevZrno;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;

@Interceptor
@BeleziKlice
public class BelezenjeKlicevInterceptor {

    private static Logger logger = Logger.getLogger(BelezenjeKlicevInterceptor.class.getName());

    @Inject
    private BelezenjeKlicevZrno bkz;

    @AroundInvoke
    public Object aroundInvoke(InvocationContext context) throws Exception{
        bkz.setCounter(bkz.getCounter()+1);
        logger.info("COUNTER = "+bkz.getCounter());
        return context.proceed();
    }

}
