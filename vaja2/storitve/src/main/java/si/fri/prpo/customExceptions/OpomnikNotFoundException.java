package si.fri.prpo.customExceptions;

import javax.persistence.NoResultException;

public class OpomnikNotFoundException extends NoResultException {

    public OpomnikNotFoundException(){
        super();
    }
}
