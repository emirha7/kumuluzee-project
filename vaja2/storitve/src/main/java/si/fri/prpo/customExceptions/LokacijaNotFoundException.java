package si.fri.prpo.customExceptions;

import javax.persistence.NoResultException;

public class LokacijaNotFoundException extends NoResultException {

    public LokacijaNotFoundException(){
        super();
    }
}