package si.fri.prpo.zrna;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.RequestScoped;
import java.util.UUID;
import java.util.logging.Logger;

@RequestScoped
public class UUIDGenerator {
    private UUID uuid = UUID.randomUUID();
    private static Logger logger = Logger.getLogger(UUIDGenerator.class.getName());
    public UUID getUUID(){
        return uuid;
    }

    @PostConstruct
    public void onCreate(){
        logger.info("UUIDGenerator ZRNO USTVARJENO "+getUUID().toString());
    }

    @PreDestroy
    public void onDestroy(){
        logger.info("UUIDGenerator ZRNO UBITO "+getUUID().toString());
    }
}
