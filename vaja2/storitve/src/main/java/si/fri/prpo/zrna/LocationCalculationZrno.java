package si.fri.prpo.zrna;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@ApplicationScoped
public class LocationCalculationZrno {


    private Client httpClient;
    private final String baseURLFormat = "https://eu1.locationiq.com/v1/search.php?key=5bdb5576ba19a2&q=%s&format=json";

    @PostConstruct
    private void init(){
        httpClient = ClientBuilder.newClient();
    }

    public String callApi(String address){
        System.out.println("address");
        System.out.println(address.replace(" ","%20"));
        return httpClient.target(String.format(baseURLFormat,address.replace(" ","%20"))).request(MediaType.APPLICATION_JSON).get(String.class);
    }
}
