package si.fri.prpo.zrna;

import si.fri.prpo.DTOObjects.StoritevDTO;
import si.fri.prpo.customExceptions.NoServiceException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import java.util.List;

@RequestScoped
public class StoritevZrno {

    private Client httpClient;
    private String baseURL;

    @Inject
    private ETCDZrno etcdZrno;

    @PostConstruct
    private void init(){
        httpClient = ClientBuilder.newClient();
        baseURL = etcdZrno.getPonudnikUrl();
    }

    public StoritevDTO getServiceByID(int id){
        System.out.println("----------------------");
        System.out.println(baseURL);
        GenericType<StoritevDTO> genericType = new GenericType<StoritevDTO>() {};
        StoritevDTO storitevDTO = httpClient.target(baseURL+id).request().get(genericType);
        if(storitevDTO == null){
            throw new NoServiceException();
        }
        return storitevDTO;
    }

    public List<StoritevDTO> getAllServices(){
        System.out.println("----------------------");
        System.out.println(baseURL);
        GenericType<List<StoritevDTO>> listGenericType = new GenericType<List<StoritevDTO>>() {};
        List<StoritevDTO> storitevDTOList = httpClient.target(baseURL).request().get(listGenericType);
        if(storitevDTOList == null){
            throw new NoServiceException();
        }
        return storitevDTOList;
    }

}
