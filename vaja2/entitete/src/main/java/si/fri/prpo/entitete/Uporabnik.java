package si.fri.prpo.entitete;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.List;

@Entity(name = "uporabnik")
@NamedQueries(value =
        {
                @NamedQuery(name = "Uporabnik.getAll", query = "SELECT u FROM uporabnik u"),
                @NamedQuery(name = "Uporabnik.getByName", query = "SELECT u FROM uporabnik u WHERE u.ime = :name"),
                @NamedQuery(name = "Uporabnik.getByUsername", query = "SELECT u FROM uporabnik u WHERE u.uporabniskoIme = :username"),
                @NamedQuery(name = "Uporabnik.getGeslo", query = "SELECT u FROM uporabnik u WHERE u.uporabniskoIme = :username AND u.geslo = :geslo")
        }
)
public class Uporabnik {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uporabnik_id;

    private String ime;

    private String priimek;

    private String email;

    @Column(name="username")
    private String uporabniskoIme;

    private String geslo;

    @OneToMany
    @JoinColumn(name = "uporabnik_id")
    private List <Opomnik> listOpomnikov;

    // getter in setter metode
    public void setId(int id) {
        this.uporabnik_id = id;
    }
    public int getId() {
        return this.uporabnik_id;
    }

    public Integer getUporabnik_id() {
        return uporabnik_id;
    }

    public void setUporabnik_id(Integer uporabnik_id) {
        this.uporabnik_id = uporabnik_id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public void setListOpomnikov(List<Opomnik> listOpomnikov) {
        this.listOpomnikov = listOpomnikov;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    public String getUporabniskoIme() {
        return uporabniskoIme;
    }

    public void setUporabniskoIme(String uporabniskoIme) {
        this.uporabniskoIme = uporabniskoIme;
    }


    @JsonbTransient
    public List<Opomnik> getListOpomnikov(){
        return listOpomnikov;
    }

    public String getGeslo(){
        return geslo;
    }

    public void setGeslo(String geslo){
        this.geslo = geslo;
    }

    public String toString(){
        return String.format("uporabnik_id = %d, ime = %s, priimek = %s, email = %s, username = %s geslo = %s",uporabnik_id,ime,priimek,email,uporabniskoIme,geslo);
    }


}
