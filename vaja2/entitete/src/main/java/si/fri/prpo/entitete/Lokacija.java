package si.fri.prpo.entitete;

import javax.persistence.*;
import java.util.List;

@Entity(name = "lokacija")
@NamedQueries(value =
        {
                @NamedQuery(name = "Lokacija.getAll", query = "SELECT l FROM lokacija l"),
                @NamedQuery(name = "Lokacija.getByRadius", query = "SELECT l FROM lokacija l WHERE l.radij >= :rad"),
                @NamedQuery(name = "Lokacija.getByLatLong", query = "SELECT l FROM lokacija l WHERE l.sirina = :sirina AND l.dolzina = :dolzina")
        })
public class Lokacija {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer lokacija_id;

    private Double sirina; //geografksa_sirina

    private Double dolzina;

    private Double radij;

    @OneToMany
    @JoinColumn(name = "lokacija_id")
    private List<Opomnik> listOpomnikov;

    // getter in setter metode

    public void setId(int id) {
        this.lokacija_id = id;
    }
    public int getId() {
        return this.lokacija_id;
    }

    public void setSirina(Double sirina) {
        this.sirina = sirina;
    }
    public Double getSirina() {
        return this.sirina;
    }

    public void setDolzina(Double dolzina) {
        this.dolzina = dolzina;
    }
    public Double getDolzina() {
        return this.dolzina;
    }

    public void setRadij(Double radij) {
        this.radij = radij;
    }
    public Double getRadij() {
        return this.radij;
    }

    public String toString(){
        return String.format("lokacija_id: %d, sirina: %4.3f, dolzina: %4.3f radij: %4.3f",lokacija_id,sirina,dolzina,radij);
    }
}
