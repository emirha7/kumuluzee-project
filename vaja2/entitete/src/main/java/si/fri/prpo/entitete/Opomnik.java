package si.fri.prpo.entitete;

import javax.persistence.*;

@Entity(name = "opomnik")
@NamedQueries(value =
        {
                @NamedQuery(name = "Opomnik.getAll", query = "SELECT o FROM opomnik o"),
                @NamedQuery(name = "Opomnik.getByDescription", query = "SELECT o FROM opomnik o WHERE o.opis LIKE :str"),
                @NamedQuery(name = "Opomnik.getByUporabnikID", query = "SELECT o FROM opomnik o WHERE o.uporabnik.uporabnik_id = :uporabnikID")
        })
public class Opomnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer opomnik_id;

    private Integer storitev_id;

    private String naslov;

    private String opis;


    @ManyToOne
    @JoinColumn(name = "lokacija_id")
    private Lokacija lokacija;

    @ManyToOne
    @JoinColumn(name = "uporabnik_id")
    private Uporabnik uporabnik;

    private String status;
    // getter in setter metode

    public void setId(int id) {
        this.opomnik_id = id;
    }
    public int getId() {
        return this.opomnik_id;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }
    public String getNaslov() {
        return this.naslov;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    public String getOpis() {
        return this.opis;
    }

    public String toString(){
        return String.format("opomnik_id: %d, naslov: %s, opis: %s uporabnik: %s, lokacija: %d, status: %s",opomnik_id,naslov,opis,uporabnik.getIme(),lokacija.getId(),status);
    }

    public void setLokacija(Lokacija lok){
        this.lokacija = lok;
    }

    public Lokacija getLokacija(){
        return this.lokacija;
    }

    public void setUporabnik(Uporabnik u){
        this.uporabnik = u;
    }

    public Uporabnik getUporabnik(){
        return this.uporabnik;
    }

    public void setStoritev_id(Integer storitev_id) {
        this.storitev_id = storitev_id;
    }

    public Integer getStoritev_id() {
        return storitev_id;
    }
}


