INSERT INTO uporabnik (ime, priimek, username, email, geslo) VALUES ('Petra', 'Kos', 'petrakos', 'petra.kos@hotmail.com', '123');
INSERT INTO uporabnik (ime, priimek, username, email, geslo) VALUES ('Miha', 'Novak', 'mihanovak', 'miha.novak@gmail.com','123');
INSERT INTO lokacija (sirina, dolzina, radij) VALUES (46.050380, 14.468966, 500.0);
INSERT INTO lokacija (sirina, dolzina, radij) VALUES (46.056195, 14.506057, 500.0);
INSERT INTO opomnik (naslov, opis, lokacija_id, uporabnik_id, status,storitev_id) VALUES ('Obišči referat', 'V referatu dvigni potrdila o vpisu.', 1, 1,'done',1);
INSERT INTO opomnik (naslov, opis, lokacija_id, uporabnik_id, status,storitev_id) VALUES ('LPP', 'Podaljšaj karto za LPP.', 2, 2,'todo',1);