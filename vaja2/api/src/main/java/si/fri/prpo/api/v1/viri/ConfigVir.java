package si.fri.prpo.api.v1.viri;

import si.fri.prpo.zrna.ETCDZrno;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RequestScoped
@Path("/config")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ConfigVir {

    @Inject
    private ETCDZrno etcdZrno;

    @GET
    public Response test(){
        String response =
                        "{" +
                            "\"urlString\": \"%s\"," +
                            "\"enabled\": %b" +
                        "}";

        response = String.format(
                response,
                etcdZrno.getPonudnikUrl(),
                etcdZrno.getPonudnikEnabled());

        return Response.ok(response).build();
    }
}
