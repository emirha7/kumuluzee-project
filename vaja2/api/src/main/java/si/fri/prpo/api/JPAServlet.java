package si.fri.prpo.api;

import si.fri.prpo.entitete.Lokacija;
import si.fri.prpo.entitete.Opomnik;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.zrna.*;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

//docker run -d --name postgres-jdbc -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=lokacijskiOpomnik -p 5432:5432 postgres:10.5
@WebServlet("/servlet")
public class JPAServlet extends HttpServlet {

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    @Inject
    private OpomnikZrno opomnikZrno;

    @Inject
    LokacijaZrno lokacijaZrno;

    @Inject
    UpravljanjeOpomnikovZrno upravljanjeOpomnikovZrno;

    @Inject
    UpravljanjeUporabnikovZrno uporavljanjeUporabnikovZrno;

    @Inject
    UpravljanjeLokacijaZrno upravljanjeLokacijaZrno;

    @Inject
    UUIDGenerator uuidGenerator;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        /*PrintWriter pw = resp.getWriter();
        List<Uporabnik> uporabniki = uporabnikiZrno.getUporabniki();
        pw.println("-------------------------UPORABNIKI-------------------------<br>");
        printUporabnikList(uporabniki,pw);

        pw.println("-------------------------OPOMNIKI-------------------------<br>");
        List<Opomnik> opomniki = opomnikZrno.getAll();
        printOpomnikList(opomniki,pw);

        pw.println("-------------------------LOKACIJA-------------------------<br>");
        List<Lokacija> lokacije = lokacijaZrno.getAll();
        printLokacijaList(lokacije,pw);*/

        /*
        pw.println("-------------------------BY NAME--------------------------<br>");
        List<Uporabnik> searchByNameList = uporabnikiZrno.getByName("Miha");
        printUporabnikList(searchByNameList,pw);

        pw.println("-------------------------BY DESCRIPTION-------------------------<br>");
        List<Opomnik> opomnikByDescList = opomnikZrno.getByDescription("%vpis%");
        printOpomnikList(opomnikByDescList,pw);

        pw.println("-------------------------BY RADIUS-------------------------<br>");
        List<Lokacija> lokacijaByRadius = lokacijaZrno.getByRadius(500.0);
        printLokacijaList(lokacijaByRadius,pw);

        pw.println("-------------------------BY CRITERIA API OPOMNIK-------------------------<br>");
        List<Opomnik> opomnikCrApi = opomnikZrno.getAllCriteria();
        printOpomnikList(opomnikCrApi,pw);

        pw.println("-------------------------BY CRITERIA API LOKACIJA-------------------------<br>");
        List<Lokacija> lokacijaCrApi = lokacijaZrno.getAllCriteria();
        printLokacijaList(lokacijaCrApi,pw);

        pw.println("-------------------------BY CRITERIA API UPORABNIK-------------------------<br>");
        List<Uporabnik> uporabnikCrApi = uporabnikiZrno.getAllCriteria();
        printUporabnikList(uporabnikCrApi,pw);

        pw.println("-------------------------EM.FIND uporabnik (id)-------------------------<br>");
        pw.println(uporabnikiZrno.pridobiUporabnika(1));

        Uporabnik u = new Uporabnik();
        u.setEmail("aa@gmail.com");
        u.setName("aa");
        u.setSurname("bb");
        u.setUsername("aa");
        uporabnikiZrno.dodajUporabnika(u);
        pw.println("-------------------------Dodal uporabnika-------------------------<br>");
        pw.println(u.toString());

        pw.println("-------------------------update uporabnik-------------------------<br>");
        Uporabnik u1 = uporabnikiZrno.pridobiUporabnika(1);
        if(u1!=null){
            u1.setEmail("petra.kos@petrol.com");
            uporabnikiZrno.posodbiUporabnika(u1);
            pw.println(uporabnikiZrno.pridobiUporabnika(1).toString());
        }

        pw.println("-------------------------brisi uporabnik-------------------------<br>");
        Uporabnik uporabnikTemp = uporabnikiZrno.pridobiUporabnika(1);
        if(uporabnikTemp!=null){
            pw.println("brisem uporabnika: "+uporabnikTemp.toString());
            uporabnikiZrno.odstraniUporabnika(uporabnikTemp);
        }

        pw.println("-------------------------situacija po odstranjevanju uporabnika-------------------------<br>");
        uporabniki = uporabnikiZrno.getUporabniki();
        printUporabnikList(uporabniki,pw);*/

        /*Lokacija novaLokacija = new Lokacija();
        novaLokacija.setDolzina(64.892);
        novaLokacija.setSirina(82.1290);
        novaLokacija.setRadij(505.2);
        pw.println("-------------------------po dodajanju lokacije-------------------------<br>");
        lokacijaZrno.dodajLokacijo(novaLokacija);
        lokacije = lokacijaZrno.getAll();
        printLokacijaList(lokacije,pw);

        Lokacija tmpLokacija = lokacijaZrno.pridobiLokacijo(1);
        pw.println("-------------------------POSODABLJAM LOKACIJO 1-------------------------<br>");
        if(tmpLokacija!=null){
            tmpLokacija.setRadij(4000.0);
            lokacijaZrno.posodbiLokacijo(tmpLokacija);
            pw.println(lokacijaZrno.pridobiLokacijo(1).toString());
        }

        Lokacija ll = lokacijaZrno.pridobiLokacijo(1);
        if(ll!=null){
            lokacijaZrno.odstraniLokacijo(ll);
            pw.println("-------------------------BRISEM LOKACIJO 1-------------------------<br>");
            lokacije = lokacijaZrno.getAll();
            printLokacijaList(lokacije,pw);
        }*/


       /* Opomnik novOpomnik = new Opomnik();
        novOpomnik.setNaslov("kupi mleko");

        novOpomnik.setOpis("kupi mleko v merkatorju");
        novOpomnik.setLokacija(lokacijaZrno.pridobiLokacijo(1));
        novOpomnik.setUporabnik(uporabnikiZrno.pridobiUporabnika(1));
        opomnikZrno.dodajOpomnik(novOpomnik);
        pw.println("DODAJANJE OPOMNIKA<br>");
        opomniki = opomnikZrno.getAll();
        printOpomnikList(opomniki,pw);

        pw.println("BRISANJE OPOMNIKOV <br>");
        Opomnik opomnikTmp = opomnikZrno.pridobiOpomnik(1);
        if(opomnikTmp!=null){
            opomnikZrno.odstraniOpomnik(opomnikTmp);
            pw.println("PO BRISANJU<br>");
            opomniki = opomnikZrno.getAll();
            printOpomnikList(opomniki,pw);
        }

        Opomnik posodobljenOpomnik = opomnikZrno.pridobiOpomnik(2);
        posodobljenOpomnik.setOpis("LPPP");
        opomnikZrno.posodbiOpomnik(posodobljenOpomnik);
        pw.println("PO UPDATE<br>");
        pw.println(opomnikZrno.pridobiOpomnik(2));*/


        /*pw.println("-------------------------dodaj opomnik DTO-------------------------<br>");
        OpomnikDTO opomnikDTO = new OpomnikDTO();
        opomnikDTO.setLokacijaID(1);
        opomnikDTO.setUporabnikID(2);
        opomnikDTO.setNaslov("Trgovina");
        opomnikDTO.setOpis("kupi mleko");
        upravljanjeOpomnikovZrno.dodajOpomnik(opomnikDTO);
        opomniki = opomnikZrno.getAll();
        printOpomnikList(opomniki,pw);

        pw.println("OPOMNIKI PETRE <br>");
        Uporabnik testU = uporabnikiZrno.pridobiUporabnika(1);
        List<Opomnik> opomnikiUporabnikaTestU = testU.getListOpomnikov();
        printOpomnikList(opomnikiUporabnikaTestU,pw);

        pw.println("-------------------------posodobi opomnik naslov-------------------------<br>");
        OpomnikDTO opomnikDTONaslov = new OpomnikDTO();
        opomnikDTONaslov.setopomnikID(1);
        opomnikDTONaslov.setNaslov("obišči faks");
        upravljanjeOpomnikovZrno.posodobiOpomnikNaslov(opomnikDTONaslov);
        opomniki = opomnikZrno.getAll();
        printOpomnikList(opomniki,pw);

        UporabnikDTO uporabnikDTO = new UporabnikDTO();
        uporabnikDTO.setUsername("mihanovak");

        pw.println("-------------------------OPOMNIKI MIHA-------------------------<br>");
        List<Opomnik> opomnikListMiha = upravljanjeOpomnikovZrno.pridobiOpomnike(uporabnikDTO);
        printOpomnikList(opomnikListMiha,pw);

        pw.println("-------------------------OPOMNIKI MIHA BRISI-------------------------<br>");
        OpomnikDTO opomnikDTOBrisi = new OpomnikDTO();
        opomnikDTO.setopomnikID(3);
        upravljanjeOpomnikovZrno.izbrisiOpomnik(opomnikDTO);
        //opomnikListMiha = upravljanjeOpomnikovZrno.pridobiOpomnike(uporabnikDTO);
        printOpomnikList(opomnikListMiha,pw);

        OpomnikDTO opomnikDTOStatus = new OpomnikDTO();
        opomnikDTOStatus.setStatus("done");
        opomnikDTOStatus.setopomnikID(2);
        upravljanjeOpomnikovZrno.markOpomnik(opomnikDTOStatus);

        pw.println("-------------------------OPOMNIKI MARK-------------------------<br>");
        opomniki = opomnikZrno.getAll();
        printOpomnikList(opomniki,pw);

        pw.println("-------------------------PRIJAVA -------------------------<br>");
        UporabnikDTO udto = new UporabnikDTO();
        udto.setGeslo("123");
        udto.setUsername("petrakos");
        if(uporavljanjeUporabnikovZrno.canLogin(udto)){
            pw.println("prijava uspesna<br>");
        }else{
            pw.println("prijava NEuspesna<br>");
        }

        LokacijaDTO lokacijaDTO = new LokacijaDTO();
        lokacijaDTO.setUporabnikID(1);
        lokacijaDTO.setDolzina(33.2);
        lokacijaDTO.setSirina(234.0);
        lokacijaDTO.setRadij(400.0);
        upravljanjeLokacijaZrno.addLocation(lokacijaDTO);

        lokacije = lokacijaZrno.getAll();
        printLokacijaList(lokacije,pw);

        uuidGenerator.getUUID();*/

    }



    static void printUporabnikList(List<Uporabnik>list, PrintWriter pw){
        for(Uporabnik u : list){
            pw.println(u.toString());
        }
    }

    static void printOpomnikList(List<Opomnik>list, PrintWriter pw){
        for(Opomnik o : list){
            pw.println(o.toString());
        }
    }

    static void printLokacijaList(List<Lokacija>list, PrintWriter pw){
        for(Lokacija l : list){
            pw.println(l.toString());
        }
    }
}