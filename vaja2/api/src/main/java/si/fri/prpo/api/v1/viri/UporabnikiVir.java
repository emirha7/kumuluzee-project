package si.fri.prpo.api.v1.viri;

import com.kumuluz.ee.cors.annotations.CrossOrigin;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import si.fri.prpo.DTOObjects.UporabnikDTO;
import si.fri.prpo.DTOObjects.UporabnikOpomnikDTO;
import si.fri.prpo.entitete.Opomnik;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.zrna.UporabnikiZrno;
import si.fri.prpo.zrna.UpravljanjeUporabnikovZrno;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@ApplicationScoped
@Path("uporabniki")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@CrossOrigin(supportedMethods = "GET, POST, PUT, DELETE, HEAD, OPTIONS", allowOrigin = "*")
public class UporabnikiVir {

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    @Inject
    private UpravljanjeUporabnikovZrno upravljanjeUporabnikovZrno;

    @Context
    protected UriInfo uriInfo;

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;

    /*@GET
    public Response pridobiUporabnike(){
        System.out.println("HAAAAAAAAAAA!!!!!!!");

        return Response.ok(uporabnikiZrno.getAllCriteria()).build();
    }*/

    @GET
    //@PermitAll
    public Response vrniUporabnike(){
        /*
            http://localhost:8080/v1/uporabniki/?filter=username:EQ:mihanovak
            http://localhost:8080/v1/uporabniki/?filter=uporabnik_id:EQ:2
            http://localhost:8080/v1/uporabniki/?order=uporabnik_id DESC
            http://localhost:8080/v1/uporabniki/?order=username ASC,email DESC
         */
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();

        List<Uporabnik> uporabniki = upravljanjeUporabnikovZrno.vrniUporabnike(query);

        if(uporabniki == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }


        Long uporabnikiCount = JPAUtils.queryEntitiesCount(em, Uporabnik.class, query);
        return Response.ok(uporabniki).header("X-Total-Count", uporabnikiCount).build();
        //return Response.ok(uporabniki).header("X-Total-Count", uporabnikiCount).build();
    }

    @GET
    @Path("/{id}")
    public Response vrniPodrobnosti(@PathParam("id") Integer id){
        Uporabnik uporabnik = upravljanjeUporabnikovZrno.pridobiUporabnikaByID(id);

        for(Opomnik o: uporabnik.getListOpomnikov()){
            System.out.println(o);
        }
        UporabnikOpomnikDTO uporabnikOpomnikDTO = new UporabnikOpomnikDTO(uporabnik, uporabnik.getListOpomnikov());
        return Response.ok(uporabnikOpomnikDTO).build();
    }

    @POST
    public Response dodajUporabnika(UporabnikDTO uporabnikDTO){
        if(upravljanjeUporabnikovZrno.dodajUporabnika(uporabnikDTO)){
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Path("{id}")
    @DELETE
    public Response odstraniUporabnika(@PathParam("id") Integer id) {
        upravljanjeUporabnikovZrno.odstraniUporabnika(id);
        return Response.status(Response.Status.OK).build();
    }
}
