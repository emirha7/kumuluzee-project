package si.fri.prpo.api.v1.viri;

import si.fri.prpo.zrna.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("opomnikiAPI")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class LocationAPIVir {

    @Inject
    private LocationCalculationZrno locationCalculationZrno;

    @Inject
    private UpravljanjeUporabnikovZrno upravljanjeUporabnikovZrno;

    @Inject
    private UpravljanjeLokacijaZrno upravljanjeLokacijaZrno;

    @Inject
    private UpravljanjeOpomnikovZrno upravljanjeOpomnikovZrno;

    @GET
    public Response neki(){
        return Response.ok("HADUKEN!").build();
    }

    /*
    @POST
    public Response calculateDistance(OpomnikDTO opomnikDTO){

        LokacijaDTO lokacijaDTO = new LokacijaDTO();
        lokacijaDTO.setUporabnikID(opomnikDTO.getUporabnikID());


        String json = locationCalculationZrno.callApi(opomnikDTO.getAddress());
        JSONParser parser = new JSONParser();
        try{

            JSONArray arr = (JSONArray) parser.parse(json);
            JSONObject firstElement = (JSONObject) arr.get(0);
            System.out.println("LAT: " +firstElement.get("lat"));
            System.out.println("LONG: " +firstElement.get("lon"));

            double lat = Double.parseDouble((String)firstElement.get("lat"));
            double lon = Double.parseDouble((String)firstElement.get("lon"));

            lokacijaDTO.setSirina(lat);
            lokacijaDTO.setDolzina(lon);
            lokacijaDTO.setRadij(500);

            int lokacijaID = upravljanjeLokacijaZrno.addLocation(lokacijaDTO);
            opomnikDTO.setLokacijaID(lokacijaID);

            upravljanjeOpomnikovZrno.dodajOpomnik(opomnikDTO);
        }catch (Exception e){
            e.printStackTrace();
        }

        return Response.ok(json).build();
    }*/
}
