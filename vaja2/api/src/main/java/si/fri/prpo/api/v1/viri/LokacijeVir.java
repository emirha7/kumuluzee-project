package si.fri.prpo.api.v1.viri;

import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import com.kumuluz.ee.security.annotations.Secure;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import si.fri.prpo.anotacije.BeleziKlice;
import si.fri.prpo.entitete.Lokacija;
import si.fri.prpo.DTOObjects.LokacijaDTO;
import si.fri.prpo.zrna.LokacijaZrno;
import si.fri.prpo.zrna.UpravljanjeLokacijaZrno;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Secure
@Path("lokacije")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
@BeleziKlice
public class LokacijeVir {

    @Inject
    private UpravljanjeLokacijaZrno upravljanjeLokacijaZrno;

    @Inject
    private LokacijaZrno lokacijaZrno;

    @Context
    protected UriInfo uriInfo;

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;

    @Operation(
            description = "Doda lokacijo glede na LokacijaDTO objekt.",
            summary = "Doda lokacijo glede na LokacijaDTO objekt, podan kot body.",
            tags = "lokacije",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno dodana lokacija."
                    )
            }
    )
    @POST
    @Path("dodajLokacijo")
    @RolesAllowed("administrator")
    public Response dodajLokacijo(@Parameter(description = "Objekt lokacijaDTO",required = true) LokacijaDTO lokacijaDTO){
        if(upravljanjeLokacijaZrno.addLocation(lokacijaDTO) != -1){
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    @Operation(
            description = "Vrne seznam vseh lokacij.",
            summary = "Pridobi seznam vseh lokacij in vse njihove informacije (dolzina, id, radij, sirina)",
            tags = "lokacije",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno pridobljen seznam lokacij",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type ="array", example = "[\n" +
                                            "    {\n" +
                                            "        \"dolzina\": 14.468966,\n" +
                                            "        \"id\": 1,\n" +
                                            "        \"radij\": 500,\n" +
                                            "        \"sirina\": 46.05038\n" +
                                            "    },\n" +
                                            "    {\n" +
                                            "        \"dolzina\": 14.506057,\n" +
                                            "        \"id\": 2,\n" +
                                            "        \"radij\": 500,\n" +
                                            "        \"sirina\": 46.056195\n" +
                                            "    }\n" +
                                            "]")
                            ),
                            headers = {@Header(name = "X-Total-Count", schema = @Schema(type = "integer"))}
                    ),
                    @ApiResponse(responseCode =  "400",
                            description = "Neobstoječe polje v url parametrih ali pa neveljaven format",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type="object", example = "{\n" +
                                            "\"errorMessage\": \"Object not found exception\",\n" +
                                            "\"type\": \"NoSuchEntityFieldException\" \n" +
                                            "}")

                            )
                    ),
                    @ApiResponse(responseCode =  "400",
                            description = "Neustrezen format poizvedbe",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type="object", example = "{\n" +
                                            "  \"errorMessage\": \"Query exception\",\n" +
                                            "  \"type\": \"QueryFormatException\"\n" +
                                            "}")
                            )
                    ),
                    @ApiResponse(responseCode =  "404",
                            description = "Seznama lokacij ni bilo mogoče najti",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type="object", example = "{\n" +
                                            "\"errorMessage\": \"Object not found exception\",\n" +
                                            "\"type\": \"UserNotFoundException\" \n" +
                                            "}")
                            )
                    )
            }
    )
    @GET
    @PermitAll
    public Response vrniLokacije(){
        /*
            http://localhost:8080/v1/lokacije?filter=lokacija_id:EQ:1
            http://localhost:8080/v1/lokacije?filter=radij:GT:200
            http://localhost:8080/v1/lokacije?order=radij ASC
            http://localhost:8080/v1/lokacije?limit=1&offset=1
         */
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();
        List<Lokacija> lokacijaList = upravljanjeLokacijaZrno.vrniLokacije(query);

        if(lokacijaList == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        Long lokacijeCount = JPAUtils.queryEntitiesCount(em, Lokacija.class, query);

        return Response.ok(lokacijaList).header("X-Total-Count", lokacijeCount).build();
    }
    @Operation(
            description = "Izbrise lokacijo s podanim id-jem.",
            summary = "Izbrise lokacijo z id-jem, ki ga podamo kot parameter v path-u.",
            tags = "lokacije",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno izbrisana lokacija."
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Lokacije s podanim id-jem ni bilo mozno izbrisati.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "       \"errorMessage\": \"Object not found exception\",\n" +
                                            "       \"type\": \"LokacijaNotFoundException\" \n" +
                                            "   }\n" +
                                            "]")
                            )
                    )
            }
    )
    @Path("/{id}")
    @DELETE
    @RolesAllowed("administrator")
    public Response izbrisiLokacijo(@PathParam("id") int id){
        lokacijaZrno.odstraniLokacijo(lokacijaZrno.pridobiLokacijo(id));
        return Response.status(Response.Status.OK).build();
    }


}
