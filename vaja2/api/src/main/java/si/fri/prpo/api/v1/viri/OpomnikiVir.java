package si.fri.prpo.api.v1.viri;

import com.kumuluz.ee.cors.annotations.CrossOrigin;
import com.kumuluz.ee.rest.beans.QueryParameters;
import com.kumuluz.ee.rest.utils.JPAUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import si.fri.prpo.DTOObjects.OpomnikStoritevDTO;
import si.fri.prpo.anotacije.BeleziKlice;
import si.fri.prpo.entitete.Opomnik;
import si.fri.prpo.entitete.Uporabnik;
import si.fri.prpo.DTOObjects.OpomnikDTO;
import si.fri.prpo.zrna.ETCDZrno;
import si.fri.prpo.zrna.OpomnikZrno;
import si.fri.prpo.zrna.UporabnikiZrno;
import si.fri.prpo.zrna.UpravljanjeOpomnikovZrno;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

//@Secure
@Path("opomniki")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
@BeleziKlice
@CrossOrigin(supportedMethods = "GET, POST, PUT, DELETE, HEAD, OPTIONS", allowOrigin = "*")
public class OpomnikiVir {

    @Inject
    private UpravljanjeOpomnikovZrno upravljanjeOpomnikovZrno;

    @Inject
    private ETCDZrno etcdZrno;

    @Inject
    private OpomnikZrno opomnikZrno;

    @Inject
    private UporabnikiZrno uporabnikiZrno;

    @Context
    protected UriInfo uriInfo;

    @PersistenceContext(unitName = "lokacijski-opomniki-jpa")
    private EntityManager em;

    @Operation(
            description = "Doda opomnik glede na OpomnikDTO objekt.",
            summary = "Doda opomnik glede na OpomnikDTO objekt, podan kot body.",
            tags = "opomniki",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno dodan opomnik."
                    )
            }
    )
    @POST
    @Path("dodajOpomnik")
    @RolesAllowed("administrator")
    public Response dodajOpomnik(@Parameter(description = "Objekt opomnikDTO",required = true) OpomnikDTO opomnikDTO){
        if(upravljanjeOpomnikovZrno.dodajOpomnik(opomnikDTO)){
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @Operation(
            description = "Vrne seznam opomnikov.",
            summary = "Pridobi seznam vseh opomnikov in vse njihove informacije (opomnik_id, naslov, opis, lokacija, uporabnik)",
            tags = "opomniki",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno pridobljen seznam opomnikov.",
                                content = @Content(mediaType = "application/json",
                                    schema = @Schema(type ="array", example = "[\n" +
                                            "    {\n" +
                                            "        \"id\": 1,\n" +
                                            "        \"lokacija\": {\n" +
                                            "            \"dolzina\": 14.468966,\n" +
                                            "            \"id\": 1,\n" +
                                            "            \"radij\": 500,\n" +
                                            "            \"sirina\": 46.05038\n" +
                                            "        },\n" +
                                            "        \"naslov\": \"Obišči referat\",\n" +
                                            "        \"opis\": \"V referatu dvigni potrdila o vpisu.\",\n" +
                                            "        \"status\": \"done\",\n" +
                                            "        \"uporabnik\": {\n" +
                                            "            \"email\": \"petra.kos@hotmail.com\",\n" +
                                            "            \"geslo\": \"123\",\n" +
                                            "            \"id\": 1,\n" +
                                            "            \"name\": \"Petra\",\n" +
                                            "            \"surname\": \"Kos\",\n" +
                                            "            \"username\": \"petrakos\"\n" +
                                            "        }\n" +
                                            "    },\n" +
                                            "    {\n" +
                                            "        \"id\": 2,\n" +
                                            "        \"lokacija\": {\n" +
                                            "            \"dolzina\": 14.506057,\n" +
                                            "            \"id\": 2,\n" +
                                            "            \"radij\": 500,\n" +
                                            "            \"sirina\": 46.056195\n" +
                                            "        },\n" +
                                            "        \"naslov\": \"LPP\",\n" +
                                            "        \"opis\": \"Podaljšaj karto za LPP.\",\n" +
                                            "        \"status\": \"todo\",\n" +
                                            "        \"uporabnik\": {\n" +
                                            "            \"email\": \"miha.novak@gmail.com\",\n" +
                                            "            \"geslo\": \"123\",\n" +
                                            "            \"id\": 2,\n" +
                                            "            \"name\": \"Miha\",\n" +
                                            "            \"surname\": \"Novak\",\n" +
                                            "            \"username\": \"mihanovak\"\n" +
                                            "        }\n" +
                                            "    }\n" +
                                            "]")
                            ),
                            headers = {@Header(name = "X-Total-Count", schema = @Schema(type = "integer"))}
                    ),
                    @ApiResponse(responseCode = "400",
                            description = "Neobstoječe polje v path parametrih.",
                                content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "      \"errorMessage\": \"Field not found\",\n" +
                                            "      \"type\": \"NoSuchEntityFieldException\"\n" +
                                            "   }\n" +
                                            "]")
                                )
                    ),
                    @ApiResponse(responseCode =  "400",
                            description = "Neustrezen format poizvedbe",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type="object", example = "{\n" +
                                            "  \"errorMessage\": \"Query exception\",\n" +
                                            "  \"type\": \"QueryFormatException\"\n" +
                                            "}")
                            )
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Seznama opomnikov ni bilo mogoče najti.",
                                content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "       \"errorMessage\": \"Object not found exception\",\n" +
                                            "       \"type\": \"OpomnikNotFoundException\" \n" +
                                            "   }\n" +
                                            "]")
                            )
                    )
            }
    )
    @GET
    public Response vrniOpomnike() {
          /*
            http://localhost:8080/v1/opomniki/?filter=opomnik_id:EQ:1
            http://localhost:8080/v1/opomniki/?filter=uporabnik.uporabnik_id:EQ:1
            http://localhost:8080/v1/opomniki/?filter=uporabnik.username:EQ:mihanovak
         */
        QueryParameters query = QueryParameters.query(uriInfo.getRequestUri().getQuery()).build();

        if (etcdZrno.getPonudnikEnabled()) {
            List<OpomnikStoritevDTO> opomnikStoritevList = upravljanjeOpomnikovZrno.vrniOpomnikeStoritev(query);

            if (opomnikStoritevList == null) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            Long opomnikiStoritveCount = JPAUtils.queryEntitiesCount(em, Opomnik.class, query);
            return Response.ok(opomnikStoritevList).header("X-Total-Count", opomnikiStoritveCount).build();

        }

        List<Opomnik> opomnikList = upravljanjeOpomnikovZrno.vrniOpomnike(query);
        Long opomnikiCount = JPAUtils.queryEntitiesCount(em, Opomnik.class, query);
        return Response.ok(opomnikList).header("X-Total-Count", opomnikiCount).build();
    }


    @Operation(
            description = "Vrne seznam opomnikov uporabnika s podanim uporabniškim imenom.",
            summary = "Pridobi seznam vseh opomnikov uporabnika s podanim uporabniškim imenom in vse njihove informacije (opomnik_id, naslov, opis, lokacija, uporabnik)",
            tags = "opomniki",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno pridobljen seznam opomnikov uporabnika s podanim uporabniškim imenom.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type ="array", example = "[\n" +
                                            "    {\n" +
                                            "        \"id\": 2,\n" +
                                            "        \"lokacija\": {\n" +
                                            "            \"dolzina\": 14.506057,\n" +
                                            "            \"id\": 2,\n" +
                                            "            \"radij\": 500,\n" +
                                            "            \"sirina\": 46.056195\n" +
                                            "        },\n" +
                                            "        \"naslov\": \"LPP\",\n" +
                                            "        \"opis\": \"Podaljšaj karto za LPP.\",\n" +
                                            "        \"status\": \"todo\",\n" +
                                            "        \"uporabnik\": {\n" +
                                            "            \"email\": \"miha.novak@gmail.com\",\n" +
                                            "            \"geslo\": \"123\",\n" +
                                            "            \"id\": 2,\n" +
                                            "            \"name\": \"Miha\",\n" +
                                            "            \"surname\": \"Novak\",\n" +
                                            "            \"username\": \"mihanovak\"\n" +
                                            "        }\n" +
                                            "    }\n" +
                                            "]")
                            ),
                            headers = {@Header(name = "X-Total-Count", schema = @Schema(type = "integer"))}
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Seznama opomnikov uporabnika s podanim uporabniškim imenom ni bilo mogoče najti.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "       \"errorMessage\": \"Object not found exception\",\n" +
                                            "       \"type\": \"OpomnikNotFoundException\" \n" +
                                            "   }\n" +
                                            "]")
                            )
                    )
            }
    )
    @GET
    @Path("vrniOpomnike/{id}")
    public Response vrniOpomnike(@PathParam("id")Integer id){
        Uporabnik u = uporabnikiZrno.pridobiUporabnika(id);
        if(u == null){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        List<Opomnik> opomnikList = opomnikZrno.pridobiOpomnikeByUporabnikID(u.getId());
        if(opomnikList == null){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        if(etcdZrno.getPonudnikEnabled()){
            List<OpomnikStoritevDTO> opomnikStoritevDTOS = new ArrayList<OpomnikStoritevDTO>();
            for(Opomnik o : opomnikList){
                opomnikStoritevDTOS.add(opomnikZrno.pridobiOpomnikStoritev(o.getId()));
            }
            return Response.status(Response.Status.OK).header("X-Total-Count",opomnikStoritevDTOS.size()).entity(opomnikStoritevDTOS).build();
        }
        return Response.status(Response.Status.OK).header("X-Total-Count",opomnikList.size()).entity(opomnikList).build();
    }

    @Operation(
            description = "Posodobi opomnik glede na OpomnikDTO objekt.",
            summary = "Posodobi opomnik glede na OpomnikDTO objekt, glede na podan atribut in vrednost le-tega.",
            tags = "opomniki",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno posodobljen opomnik."
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Opomnika s podanim id-jem in OpomnikDTO objektom ni bilo mozno najti.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "       \"errorMessage\": \"Object not found exception\",\n" +
                                            "       \"type\": \"OpomnikNotFoundException\" \n" +
                                            "   }\n" +
                                            "]")
                            )
                    )
            }
    )
    @PUT
    @Path("posodobiOpomnik")
    public Response posodobiOpomnik(@Parameter(description = "Objekt opomnikDTO",required = true) OpomnikDTO opomnikDTO){
        upravljanjeOpomnikovZrno.posodobiOpomnik(opomnikDTO);
        return Response.status(Response.Status.OK).build();
    }

    @Operation(
            description = "Izbrise opomnik s podanim id-jem.",
            summary = "Izbrise opomnik z id-jem, ki ga podamo kot parameter v path-u.",
            tags = "opomniki",
            responses = {
                    @ApiResponse(responseCode =  "200",
                            description = "Uspesno izbrisan opomnik.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type ="boolean", example = "true")
                            )
                    ),
                    @ApiResponse(responseCode = "404",
                            description = "Opomnika s podanim id-jem ni bilo mozno izbrisati.",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(type = "array", example = "[\n" +
                                            "   {\n" +
                                            "       \"errorMessage\": \"Object not found exception\",\n" +
                                            "       \"type\": \"OpomnikNotFoundException\" \n" +
                                            "   }\n" +
                                            "]")
                            )
                    )
            }
    )
    @DELETE
    @Path("/{id}")
    //@RolesAllowed("administrator")
    public Response odstraniOpomnik(@PathParam("id") int id){
        return Response.ok(upravljanjeOpomnikovZrno.izbrisiOpomnik(id)).build();
    }





}