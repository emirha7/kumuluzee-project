package si.fri.prpo.api.v1.viri;

import si.fri.prpo.DTOObjects.StoritevDTO;
import si.fri.prpo.zrna.StoritevZrno;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("storitve")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class StoritevVir {

    @Inject
    private StoritevZrno storitevZrno;


    @GET
    @Path("{id}")
    public Response getService(@PathParam("id")int id){
        StoritevDTO storitevDTO = storitevZrno.getServiceByID(id);
        return Response.ok(storitevDTO).build();
    }

    @GET
    public Response getAllServices(){
        List<StoritevDTO> storitevDTOList = storitevZrno.getAllServices();
        return Response.ok(storitevDTOList).build();
    }

}
