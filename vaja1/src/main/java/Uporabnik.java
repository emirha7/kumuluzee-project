public class Uporabnik extends Entiteta {
    private String uporabnikIme;
    private String uporabnikPriimek;
    private String uporabnikEmail;
    private String uporabnikUsername;
    //še kkšn location stamp ki ga preverja na določen interval;

    public Uporabnik(String uporabnikIme, String uporabnikPriimek, String uporabnikEmail, String uporabnikUsername, int uporabnikID) {
        this.uporabnikUsername = uporabnikUsername;
        this.id = uporabnikID;
        this.uporabnikEmail = uporabnikEmail;
        this.uporabnikIme = uporabnikIme;
        this.uporabnikPriimek = uporabnikPriimek;
    }

    public Uporabnik(String uporabnikIme, String uporabnikPriimek, String uporabnikEmail, String uporabnikUsername) {
        this.uporabnikUsername = uporabnikUsername;
        this.uporabnikEmail = uporabnikEmail;
        this.uporabnikIme = uporabnikIme;
        this.uporabnikPriimek = uporabnikPriimek;
    }

    public String toString() {
        return String.format("ime = %s, priimek = %s, uporabniskoIme = %s, email = %s, id = %d\n",
                uporabnikIme, uporabnikPriimek, uporabnikUsername, uporabnikEmail, id);
    }

    public String getUporabnikIme() { return uporabnikIme; }

    public String getUporabnikPriimek() {
        return uporabnikPriimek;
    }

    public void setUporabnikIme(String ime) {
        this.uporabnikIme = ime;
    }

    public String getUporabnikEmail() {
        return uporabnikEmail;
    }

    public void setUporabnikUsername(String username) {
        this.uporabnikUsername = username;
    }

    public String getUporabnikUsername() {
        return uporabnikUsername;
    }

    public void setUporabnikEmail(String email) {
        this.uporabnikEmail = email;
    }

    public void setUporabnikPriimek(String priimek) {
        this.uporabnikPriimek = priimek;
    }


}