import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UporabnikDao implements BaseDao {

    private static UporabnikDao ourInstance = new UporabnikDao();
    private Connection con;
    private Logger log = Logger.getLogger(UporabnikDao.class.getName());

    public static UporabnikDao getInstance() {
        return ourInstance;
    }

    private UporabnikDao() {
    }

    @Override
    public Connection getConnection() {
        try {
            InitialContext initCtx = new InitialContext();
            DataSource ds = (DataSource) initCtx.lookup("jdbc/SimpleJdbcDS");
            return ds.getConnection();
        } catch (Exception e) {
            log.info("Connection error: "+e.toString());
        }

        return null;
    }

    private Uporabnik getUporabnikFromRS(ResultSet rs) throws SQLException {

        String ime = rs.getString("ime");
        String priimek = rs.getString("priimek");
        String uporabniskoIme = rs.getString("uporabniskoime");
        String email = rs.getString("email");
        int id = rs.getInt("uporabnikid");
        return new Uporabnik(ime, priimek, uporabniskoIme,email,id);

    }

    @Override
    public Entiteta vrni(int id) {

        PreparedStatement ps = null;

        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabnik WHERE uporabnikid = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return getUporabnikFromRS(rs);
            } else {
                log.info("Uporabnik ne obstaja");
            }

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
        return null;
    }

    @Override
    public void vstavi(Entiteta ent) {
        PreparedStatement ps = null;
        Uporabnik u = (Uporabnik)ent;
        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "INSERT INTO uporabnik"
                    + "(ime, priimek, email,uporabniskoime) VALUES"
                    + "(?,?,?,?)";

            ps = con.prepareStatement(sql);

            ps.setString(1,u.getUporabnikIme());
            ps.setString(2,u.getUporabnikPriimek());
            ps.setString(3,u.getUporabnikEmail());
            ps.setString(4,u.getUporabnikUsername());

            ps.executeUpdate();
        } catch (SQLException e) {
            log.severe("vstavi: "+e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
    }

    @Override
    public void odstrani(int id) {
        PreparedStatement ps = null;

        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "DELETE FROM uporabnik WHERE uporabnikid = ?";

            ps = con.prepareStatement(sql);
            ps.setInt(1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            log.severe("odstrani: "+e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }

    }

    @Override
    public void posodobi(Entiteta ent) {
        PreparedStatement ps = null;
        Uporabnik u = (Uporabnik)ent;
        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "UPDATE uporabnik SET ime = ?, uporabnikid = ?, email = ?, uporabniskoime = ?, priimek = ? WHERE uporabnikid = ?";


            ps = con.prepareStatement(sql);
            ps.setString(1,u.getUporabnikIme());
            ps.setInt(2,u.getId());
            ps.setString(3,u.getUporabnikEmail());
            ps.setString(4,u.getUporabnikUsername());
            ps.setString(5,u.getUporabnikPriimek());
            ps.setInt(6,u.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            log.severe("posodobi: "+e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }
    }

    @Override
    public List<Entiteta> vrniVse() {
        List<Entiteta> arrayList = new ArrayList<Entiteta>();

        PreparedStatement ps = null;

        try {

            if (con == null) {
                con = getConnection();
            }

            String sql = "SELECT * FROM uporabnik";
            ps = con.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                arrayList.add(getUporabnikFromRS(rs));
            }

        } catch (SQLException e) {
            log.severe(e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    log.severe(e.toString());
                }
            }
        }

        return arrayList;
    }

    public void printAll(PrintWriter pw){
        List<Entiteta> uporabnikiList = vrniVse();

        for(Entiteta e : uporabnikiList){
            pw.print(((Uporabnik)e).toString());
        }
    }
}
