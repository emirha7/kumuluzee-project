package si.fri.prpo.ponudnik.api.si.fri.prpo.ponudnik.v1;

import com.kumuluz.ee.cors.annotations.CrossOrigin;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("v1")
@CrossOrigin(allowOrigin = "*", supportedMethods = "GET, POST, PUT, DELETE, HEAD, OPTIONS")
public class PonudnikMainVir extends Application {
}
