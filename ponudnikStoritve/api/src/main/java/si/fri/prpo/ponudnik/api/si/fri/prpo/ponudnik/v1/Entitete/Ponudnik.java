package si.fri.prpo.ponudnik.api.si.fri.prpo.ponudnik.v1.Entitete;

public class Ponudnik {

    private int id;
    private String description;
    private String title;

    public Ponudnik(int id, String description, String title) {
        this.id = id;
        this.description = description;
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
