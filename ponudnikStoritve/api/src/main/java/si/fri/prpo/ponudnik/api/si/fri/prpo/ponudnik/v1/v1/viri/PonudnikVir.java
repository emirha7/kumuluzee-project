package si.fri.prpo.ponudnik.api.si.fri.prpo.ponudnik.v1.v1.viri;

import com.kumuluz.ee.cors.annotations.CrossOrigin;
import si.fri.prpo.ponudnik.api.si.fri.prpo.ponudnik.v1.Entitete.Ponudnik;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Path("storitve")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
@CrossOrigin(supportedMethods = "GET, POST, PUT, DELETE, HEAD, OPTIONS", allowOrigin = "*")
public class PonudnikVir {

    private Logger log = Logger.getLogger(PonudnikVir.class.getName());
    private Ponudnik fri = new Ponudnik(1, "Fakulteta za računalništvo in informatiko","FRI");
    private Ponudnik lpp = new Ponudnik(2, "Ljubljanski potniški promet", "LPP");

    @GET
    @Path("{id}")
    public Response getEntity(@PathParam("id") int id){
        if(id == 1){
            log.info("Pridobivam aplikacijo z idjem 1");
            return Response.ok(fri).build();
        }

        if(id == 2){
            log.info("Pridobivam lokacijo z idjem 2");
            return Response.ok(lpp).build();
        }

        return null;
    }

    @GET
    public Response getEntities(){
        List<Ponudnik> ponudnikList = new ArrayList<Ponudnik>();
        ponudnikList.add(fri);
        ponudnikList.add(lpp);
        log.info("Vracam vse ...");
        return Response.ok(ponudnikList).build();
    }

}
