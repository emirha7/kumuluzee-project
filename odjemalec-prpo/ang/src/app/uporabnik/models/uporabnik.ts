import {Opomnik} from './opomnik';

export class Uporabnik {
		id: number;
    ime: string;
		priimek: string;
		uporabniskoIme: string;
		opomnikList: Opomnik[];
}
